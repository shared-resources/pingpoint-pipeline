import groovy.transform.Field

// PRODUCT NAME to use for pipeline
@Field def product_name = "pingpoint"
// RELEASE VERSION to use for pipeline
@Field def release_version = "0.1"
// Folder name to use to group set of jobs
@Field def folderName = product_name
// +'-'+release_version
@Field def release_branch = "master"

@Field def infraJobName = 'pingpoint-pipeline'
@Field def pipelineEnvs  = ['pingpoint-smk','pingpoint-prd']

@Field def extractDir  = 'pingpoint'

@Field def productRepo  = "git@bitbucket.org:pingpoint/pingpoint-landingpage.git"

@Field def remoteServerNonPrd = 'katelin-deserano-adventure-trial'

@Field def remoteServerPrd = 'prod-server'

@Field def details = [
  'default': [
    'email': 'umesh.veerasingam@gmail.com',
    'manualTrigger': '',
    'autoTrigger': '',
    'approval': 'false',
    'permission': ['UmeshVeerasingam'],
    'stageID': 'undefined'
  ],
  'pingpoint-smk': [
    'manualTrigger': 'pingpoint-prd',
    'permission': ['UmeshVeerasingam'],
    'stageID': 'test',
    'environment': remoteServerNonPrd
  ],
  'pingpoint-prd': [
    'email': 'umesh.veeraingam@gmail.com',
    'permission': ['UmeshVeerasingam'],
    'approval': 'true',
    'stageID': 'prd',
    'environment': remoteServerPrd
  ]
]

def deriveDetails(String env, String field) {
  //Any given field can override, but if not specified, it will use the default
  if (details[env]) {
    if (details[env][field]) {
      details[env][field]
    } else {
      details['default'][field]
    }
  } else {
    details['default'][field]
  }
}

folder(folderName) {
  displayName(folderName)
  description(product_name)
}

//Create pipeline start job
    freeStyleJob(folderName + '/' + infraJobName) {

      deliveryPipelineConfiguration('start', infraJobName)
      description "DO NOT MODIFY THESE JOBS! Instead update the "+ product_name+"_pipelines.groovy file inside the https://bitbucket.org/shared-resources/"+product_name+"-pipelines/overview jenkins_dsl project.  Any changes to these jobs will be overwritten by the Jenkins job DSL plugin!"
      
      logRotator {
        numToKeep(5)
        artifactNumToKeep(5)
      }

      authorization {
         deriveDetails(product_name+'-smk', 'permission').each{ user ->
           permission("hudson.model.Item.Build:$user")
           permission("hudson.model.Item.Workspace:$user")
           permission("hudson.model.Item.Configure:$user")
           permission("hudson.model.Item.Read:$user")
         }
        permission("hudson.model.Item.Read:anonymous")
      }

      parameters {
          booleanParam('PIPELINE_START', true, 'Should the next job trigger?')
          stringParam('PIPELINE_VERSION', null,  null)
      }

      scm {
          git {
              remote {
                  url(productRepo)
              }
              branch(release_branch)
              extensions {
                wipeOutWorkspace()
                ignoreNotifyCommit()
              }
          }
      }
      triggers {
      }
      wrappers{
        preBuildCleanup()
        golang('Go 1.8')  
        colorizeOutput('xterm')
        timestamps()
        preBuildCleanup()
        buildUserVars()
        deliveryPipelineVersion("${release_version}.${BUILD_NUMBER}", true)
        buildName('${PIPELINE_VERSION}')
        environmentVariables{
           env('GOPATH', '$WORKSPACE')
        }
      }
      steps {
      shell(
"""#!/bin/bash -l

#!/bin/bash -l

go version

echo WORKSPACE \$WORKSPACE
echo GOPATH \$GOPATH

export PATH=\$PATH:\$GOPATH/bin

go get github.com/constabulary/gb/...

gb info

gb build

echo ============ COMMITTING INFRASTRUCTURE PACKAGE ============

echo \${PIPELINE_VERSION}
echo PIPELINE_VERSION=\$PIPELINE_VERSION
tar -cvzf ${product_name}-\$PIPELINE_VERSION.tar.gz bin/${product_name} public assets run.sh

echo ============ TAGGING =========

echo START=\$PIPELINE_START
if [ "\$PIPELINE_START" = false ]; then exit 1; fi
""")
      }
      publishers {
        archiveArtifacts {
          pattern(product_name+'-$PIPELINE_VERSION.tar.gz')
          onlyIfSuccessful()
        }
        fingerprint('',true)
        downstream(product_name+'-smk', 'SUCCESS')
        mailer(deriveDetails(product_name+'-smk', 'email'), false, true) //just pull the email notifications from the first job...
      }
}

//Create pipeline view

buildPipelineView(folderName + '/' + product_name + ' ' + ' Alt') {
    description( release_version + product_name)
    filterBuildQueue(false)
    filterExecutors(false)

    // BuildPipelineView options
    title(release_version + product_name)
    selectedJob(infraJobName)
    displayedBuilds(20)
    triggerOnlyLatestJob(false) //Should allow deploying of old versions to an env
    alwaysAllowManualTrigger(true)
    startsWithParameters(true)
    showPipelineDefinitionHeader(false) //This can be true, but just shows a large boilerplate at the top...not really useful
    showPipelineParametersInHeaders(false) //This probably needs to stay false, as there are secret keys in some job params
    showPipelineParameters(true) //Shows the grey box on the left
    refreshFrequency(20)
    customCssUrl(null)
    consoleOutputLinkStyle(OutputStyle.NewWindow) //Lightbox, NewWindow and ThisWindow.
}

deliveryPipelineView(folderName + '/' + product_name + ' ') {
    description("The view to promote releases through pipeline")
    pipelineInstances(10)
    showAggregatedPipeline()
    columns(1)
    sorting(Sorting.TITLE)
    updateInterval(60)
    enableManualTriggers()
    showAvatars()
    showChangeLog()
    pipelines {
        component(product_name+" ${release_version} Pipeline", infraJobName)
    }
}

listView(product_name){
  description(product_name)
  jobFilters {
    regex {
      regex(/${product_name}.*/)
    }
  }
  columns {
    status()
    weather()
    name()
    lastSuccess()
    lastFailure()
    lastDuration()
  }
}

//Create Pipeline enviornment deploy jobs
pipelineEnvs.each{
    def jobName = it
    freeStyleJob(folderName + '/' + jobName) {

      deliveryPipelineConfiguration(deriveDetails(jobName, 'stageID'),jobName)
      authorization {

       deriveDetails(jobName, 'permission').each{ user ->
         permission("hudson.model.Item.Build:$user")
         permission("hudson.model.Item.Delete:$user")
         permission("hudson.model.Item.Workspace:$user")
         permission("hudson.model.Item.Configure:$user")
         permission("hudson.model.Item.Read:$user")
       }
       permission("hudson.model.Item.Read:anonymous")
      }
      logRotator {
        numToKeep(5)
        artifactNumToKeep(5)
      }
      parameters {
        //Newer versions of Jenkins apparently don't pass the PIPELINE_VERSION without the parameter being specified...'
        stringParam('PIPELINE_VERSION', null, '')
      }
      description "DO NOT MODIFY THESE JOBS! Instead update the "+ product_name+"_pipelines.groovy file inside the https://bitbucket.org/shared-resources/"+product_name+"-pipelines/overview jenkins_dsl project.  Any changes to these jobs will be overwritten by the Jenkins job DSL plugin!"
      wrappers{
        colorizeOutput('xterm')
        timestamps()
        buildUserVars()
        preBuildCleanup()
        buildName('${PIPELINE_VERSION}')
        environmentVariables{
          env('CONTINUE_ON_FAIL', 'TRUE')
        }
      }
      steps {
          copyArtifacts(infraJobName) {
            includePatterns(product_name+'-$PIPELINE_VERSION.tar.gz')
            buildSelector {
              buildNumber('${PIPELINE_VERSION}')
            }
          }
          publishOverSsh {
            server(deriveDetails(jobName, 'environment')) {
                transferSet {
                    execCommand('rm -rf  /var/www/html/'+extractDir+' 2> /dev/null')
                }
            }
        }
          shell(
"""#!/bin/bash -le
echo "Tesing jenkins"
""")
      }
      publishers {
       archiveArtifacts {
          pattern(product_name+'-$PIPELINE_VERSION.tar.gz')
          onlyIfSuccessful()
       }
       
       publishOverSsh {
            server(deriveDetails(jobName, 'environment')) {
                transferSet {
                    execCommand('mkdir -p /var/www/html/'+extractDir)
                }
            }
            server(deriveDetails(jobName, 'environment')) {
                transferSet {
                    execCommand('tar xvf /var/www/html/'+product_name+'-*.tar.gz -C /var/www/html/'+extractDir)
                }
            }
            server(deriveDetails(jobName, 'environment')) {
                transferSet {
                    execCommand('systemctl restart '+product_name+'.service')
                }
            }
        }
        if (jobName != product_name+'-prd') {
            buildPipelineTrigger(deriveDetails(jobName, 'manualTrigger').split(',').collect { folderName + '/' + it }.join(','))
        }
        archiveArtifacts('*')
        fingerprint('',true)
        mailer(deriveDetails(jobName, 'email'), false, true)
        // if (jobName != product_name+'-prd') {
        //     downstream(deriveDetails(jobName, 'autoTrigger'), 'SUCCESS')
        // }
      }
  }
}
